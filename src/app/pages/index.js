import React from "react";
import Link from "next/link";
import Stack from "@lds/eden-stack";
import Grid from "@lds/eden-grid";
import Image from "@lds/eden-image";
import PrimaryButton from "@lds/eden-buttons/Primary";
import Layout from "../components/Layout";

/**
 * Because it is in the pages folder, this component will automatically be associated with a route.
 * When the "/" route is visited, the *default* export for this file will be served to the client.
 * Source: https://nextjs.org/docs/#routing
 */
const Home = () => (
  <Layout title="Home">
    <Stack gapSize="32">
      <Grid gapSize="16">
        {/**
         * Static files can be put in the "src/app/static" folder, then referenced with "/static/myfile".
         * Source: https://nextjs.org/docs/#static-file-serving-eg-images
         */}
        <Image src="/static/temple1.jpg" alt="placeholder" />
        <Image src="/static/temple2.jpg" alt="placeholder" />
        <Image src="/static/temple3.jpg" alt="placeholder" />
      </Grid>

      {/**
       * Use the <Link/> component for client-side routing.
       * Source: https://nextjs.org/docs/#with-link
       */}
      <Link href="/more" prefetch>
        <a href="/more">
          <PrimaryButton>Navigate to another page</PrimaryButton>
        </a>
      </Link>
    </Stack>
  </Layout>
);

export default Home;
